//SCRIPT para activar los TOOLTIP y POPOVER
$(function () {
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
  				interval: 3000
  			});

  			$('#contacto').on('show.bs.modal', function (e) {//se debe apuntar al data-target
  				console.log('El model se esta mostrando');

  				$('#contactob').removeClass('btn-outline-info');//se debe apuntar al botón
  				$('#contactob').addClass('btn-primary');
  				$('#contactob').prop('disabled', true);


			});
  			$('#contacto').on('shown.bs.modal', function (e) {
  				console.log('El model se mostró');
			});
			$('#contacto').on('hide.bs.modal', function (e) {
  				console.log('El model se oculta');
			});
  			$('#contacto').on('hidden.bs.modal', function (e) {
  				console.log('El model se ocultó');

  				$('#contactob').prop('disabled', false);
  				$('#contactob').removeClass('btn-primary');
  				$('#contactob').addClass('btn-outline-info');
			});

		});